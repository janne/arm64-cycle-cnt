// SPDX-License-Identifier: GPL-2.0-only
/*
 * enable user space access to the cycle counter
 *
 * Copyright (C) 2015 Janne Grunau
 */

#include <linux/module.h>

#include "pmcnt.h"

int __init init_module()
{
	on_each_cpu(enable_ccnt_read, NULL, 1);

	return 0;
}

void __exit cleanup_module()
{
}

MODULE_LICENSE("GPL");
