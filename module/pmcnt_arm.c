// SPDX-License-Identifier: GPL-2.0-only
/*
 * enable user space access to the cycle counter
 *
 * Copyright (C) 2015 Janne Grunau
 */

#include <asm/system_info.h>
#include <asm/cputype.h>
#include "pmcnt.h"

// runtime cpu identification copied from arch/arm/kernel/setup.c
static int arm_cpu_arch(void)
{
        int cpu_arch;

        if ((read_cpuid_id() & 0x000f0000) == 0x000f0000) {
                /* Revised CPUID format. Read the Memory Model Feature
                 * Register 0 and check for VMSAv7 or PMSAv7 */
                unsigned int mmfr0 = read_cpuid_ext(CPUID_EXT_MMFR0);
                if ((mmfr0 & 0x0000000f) >= 0x00000003 ||
                    (mmfr0 & 0x000000f0) >= 0x00000030)
                        cpu_arch = CPU_ARCH_ARMv7;
                else if ((mmfr0 & 0x0000000f) == 0x00000002 ||
                         (mmfr0 & 0x000000f0) == 0x00000020)
                        cpu_arch = CPU_ARCH_ARMv6;
                else
                        cpu_arch = CPU_ARCH_UNKNOWN;
        } else
                cpu_arch = CPU_ARCH_UNKNOWN;

        return cpu_arch;
}

void enable_ccnt_read(void* data)
{
#if defined(CONFIG_CPU_V6K) || defined(CONFIG_CPU_V7)
    unsigned int arch = arm_cpu_arch();
    if (arch >= CPU_ARCH_ARMv7) {
	// PMUSERENR = 1
	asm volatile ("mcr p15, 0, %0, c9, c14, 0" :: "r"(1));
	// PMCR.E (bit 0) = 1
	asm volatile ("mcr p15, 0, %0, c9, c12, 0" :: "r"(1));
	// PMCNTENSET.C (bit 31) = 1
	asm volatile ("mcr p15, 0, %0, c9, c12, 1" :: "r"(1 << 31));
    } else if (arch >= CPU_ARCH_ARMv6) {
	// ARM1176JZS specific
	// Secure User and Non-secure Access Valication Control Register = 1
	asm volatile ("mcr p15, 0, %0, c15, c9,  0" :: "r"(1));
	// PMCR.E (bit 0) = 1
	asm volatile ("mcr p15, 0, %0, c15, c12, 0" :: "r"(1));
    }
#else
#error Unsupported ARM architecture
#endif
}
