// SPDX-License-Identifier: GPL-2.0-only
/*
 * enable user space access to the cycle counter
 *
 * Copyright (C) 2015 Janne Grunau
 */

#include <linux/types.h>

#include "pmcnt.h"

#define ARM64_PMUSERENR_EN (1 << 0) // PM trap
#define ARM64_PMUSERENR_CR (1 << 2) // cycle counter read trap
#define ARM64_PMUSERENR_ER (1 << 3) // event counter read trap

void enable_ccnt_read(void* data)
{
	u64 cen, cfg = ARM64_PMUSERENR_EN | ARM64_PMUSERENR_CR | ARM64_PMUSERENR_ER;
	u64 pmcr;

	__asm__ volatile("MRS %[cen], PMCNTENSET_EL0   \n\t"
			 "MRS %[pmcr], PMCR_EL0        \n\t"
			 "MSR PMUSERENR_EL0, %[cfg]    \n\t"
			 "ORR %[cen], %[cen], 1<<31    \n\t"
			 "ORR %[pmcr], %[pmcr], 1      \n\t"
			 "MSR PMCNTENSET_EL0, %[cen]   \n\t"
			 "MSR PMCR_EL0, %[pmcr]        \n\t"
			 : [cen]"=&r"(cen), [pmcr]"=&r"(pmcr)
			 : [cfg]"r"(cfg)
			 :);
}
