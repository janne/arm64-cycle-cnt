// SPDX-License-Identifier: GPL-2.0-only
/*
 * enable user space access to the cycle counter
 *
 * Copyright (C) 2015 Janne Grunau
 */

#ifndef __ARM_PMCNT_H
#define __ARM_PMCNT_H

void enable_ccnt_read(void* data);

#endif /* __ARM_PMCNT_H */
